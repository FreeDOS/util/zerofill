/* ------------------------------------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <fcntl.h>
#include <io.h>
#include <dos.h>
#include <conio.h>


/* ------------------------------------------------------------------------------- */
#define __WATCOM_LFN__

#pragma pack (push, 1)
/* Mimic diskfree_t struct of _dos_getdiskfree with expanded disk support */
struct diskfree_ext_t
{
	unsigned short datasize;           /* size of this structure  */
	union
	{
		unsigned short requested;            /* requested structure version */
		unsigned short actual;               /* actual structure version              */
	}
	diskfree_ext_version;
	unsigned long  sectors_per_cluster;           /* number of sectors per cluster         */
	unsigned long  bytes_per_sector;            /* number of bytes per sector            */
	unsigned long  avail_clusters;       /* number of available clusters          */
	unsigned long  total_clusters;      /* total number of clusters on the drive */
	unsigned long  physical_sectors;        /* number of physical sectors available  */
	unsigned long  total_physical_sectors;       /* total number of physical sectors      */
	unsigned long  allocation_units;          /* number of available allocation units  */
	unsigned long total_allocation_units;         /* total allocation units                */
	unsigned char reserved[8];
};
#pragma pack (pop)


/* ------------------------------------------------------------------------------- */
/* Mimic _getdiskfree and _dos_getdiskfree with expanded disk support if available */
int _dos_getdiskfree_ext(unsigned drive, struct diskfree_ext_t *diskspace_ext)
{
	union REGS r;
	struct SREGS s;
	char acPath[] = "X:\\";
    
	/* Try extended disk space with 7303h service of int 21h */
	acPath[0] = 'A' + drive - 1;
	memset(diskspace_ext, 0, sizeof(struct diskfree_ext_t));
	r.x.ax = 0x7303;
	s.es = FP_SEG(diskspace_ext);
	r.x.di = FP_OFF(diskspace_ext);
	s.ds = FP_SEG(acPath);
	r.x.dx = FP_OFF(acPath);
	r.x.cx = sizeof(struct diskfree_ext_t);
	intdosx(&r, &r, &s);
	
	/* Extended Win9x FAT32 call failed or unsupported */
	/* FreeDOS: CF=0, AX=7303 on success, CF=1 on error */
	/* EDR: CF=0, AX=0 on success, CF=1 on error */
	/* MS-DOS AX=7300, CF=0 on error */
	if ((r.x.cflag) || (r.x.ax == 0x7300))
	{
		/* Use regular disk space with 36h service of int 21h */
		memset(diskspace_ext, 0, sizeof(struct diskfree_ext_t));
		r.h.ah = 0x36;
		r.h.dl = drive;
		intdosx(&r, &r, &s);
		/* If success, dump diskspace information to the structure */
		if (r.x.ax != 0xFFFF)
		{
			diskspace_ext->sectors_per_cluster = (unsigned long) r.x.ax;
			diskspace_ext->bytes_per_sector = (unsigned long) r.x.bx;
			diskspace_ext->avail_clusters = (unsigned long) r.x.cx;
			diskspace_ext->total_clusters = (unsigned long) r.x.dx;
			return(0);
		}
		else
		{
			errno = 22 /* EINVAL */;
			return(1);
		}
	}
	else
	{
		return(0);
	}
}


/* ------------------------------------------------------------------------------- */
int main (unsigned int argc, const char *argv[])
{
    int iFile, iHandle;
    unsigned int iPercent, iWritten;
	unsigned long lFileBytes;
    unsigned long long lFreeMb, lTotalMb;
    unsigned char *pcBuffer;
    struct diskfree_ext_t udtDiskFreeExt;
    char acFile[_MAX_PATH];
	char cDrive;

    /* Show header */
    cprintf("ZEROFILL R2.05        Fill with zeros empty disk space\r\n");
    cprintf("Copyright (c) 2012-2025 by Javier Gutierrez Chamorro (Guti)\r\n\r\n");
    
    /* If we have enought arguments and is a letter */
    if ((argc >= 2) && (isalpha(argv[1][0])))
    {	
    	cDrive = toupper(argv[1][0]);
       	cprintf("Filling with zero empty space on drive %c:...\r\n", cDrive);
        lTotalMb = 0;
		break_on();

		/* Get free disk space */
		if (_dos_getdiskfree_ext(cDrive - 'A' + 1, &udtDiskFreeExt) == 0)
        {
			lFreeMb = ((unsigned long long) udtDiskFreeExt.avail_clusters * udtDiskFreeExt.sectors_per_cluster * udtDiskFreeExt.bytes_per_sector) >> 20;
            pcBuffer = malloc(SHRT_MAX + 1);
            /* If we have allocated memory */
            if (pcBuffer != NULL)
            {
                memset(pcBuffer, 0, SHRT_MAX + 1);
                iFile = 0;
                iWritten = 0;
                lFileBytes = ULONG_MAX;
                /* Loop until end */
                do
                {
                    /* If first time or last file is full */
                    if (lFileBytes >= (ULONG_MAX >> 2))
                    {
                        /* Try to resume existing fills */
						do
						{
							_dos_close(iHandle);
	                        sprintf(acFile, "%c:\\ZERO%04d.FIL", cDrive, iFile);
							iFile++;
						}
						while (_dos_open(acFile, O_RDONLY, &iHandle) == 0);
                    	_dos_close(iHandle);
                    	
						/* Create a new file */
                        if (_dos_creat(acFile, _A_NORMAL, &iHandle) == 0)
                        {
                        	/* Update total counter unless on first iteration */
                        	if (iWritten != 0)
                        	{
								lTotalMb += (lFileBytes >> 20);
							}
							lFileBytes = 0;
                        }
                        else
                        {
                            cprintf("Error. Cannot create file %s.\r\n", acFile);
                        }
                    }
                    /* Write data */
                    if (_dos_write(iHandle, pcBuffer, SHRT_MAX + 1, &iWritten) != 0)
                   	{
                   		cprintf("Error. Cannot write data on %s.\r\n", acFile);
                   	}
                    lFileBytes += iWritten;
                    /* Update progress only after 1 MB */
                    if ((lFileBytes & 1048575L) == 0)
                    {
                    	iPercent = ((lFileBytes >> 20) + lTotalMb) * 50 / lFreeMb;
                    	memset(acFile, 0, sizeof(acFile));
                    	memset(acFile, 178, iPercent);
                    	memset(&acFile[iPercent], 176, 50 - iPercent);
						cprintf("\r%lldMB / %lldMB %s [%u%%]", (lFileBytes >> 20) + lTotalMb, lFreeMb, acFile, iPercent << 1);
					}
                }
                while (((lFileBytes >> 20) + lTotalMb) < lFreeMb);
                _dos_close(iHandle);
                free(pcBuffer);
				
				/* Ask for delete temporary files */
				cprintf("\r\nDo you want to delete the temporary files ZERO0000.FIL to ZERO%04d.FIL [Y/n]?", iFile - 1);
				if (toupper(getch()) != 'N')
				{
					cprintf("\r\nDeleting temporary files. This could last a while... ");
					/* Delete temporary files */
					while (iFile > 0)
					{
						sprintf(acFile, "%c:\\ZERO%04d.FIL", cDrive, iFile - 1);
						if (unlink(acFile) == 0)
						{
							cprintf("%s ", acFile);
						}
						iFile--;
					}
				}
            }
            else
            {
                cprintf("Error. Cannot allocate memory.\r\n");
            }
        }
        else
        {
        	cprintf("Error. Cannot get free disk space for %c:.\r\n", cDrive);
        }
    }
    else
    {
		/* Show help */
        cprintf("ZEROFILL [drive:]\r\n\r\n");
        cprintf("ZEROFILL writes zeros on the empty disk space for the selected drive. It helps\r\n");
        cprintf("virtual machine, and disk compression softwares to compact the volume, and so\r\n");
        cprintf("on reducing its disk usage.\r\n\r\n");
        cprintf("Examples:\r\n\r\n");
        cprintf("    ZEROFILL C:\r\n");
        cprintf("    Will fill with zeros all available space in drive C.\r\n\r\n");
        cprintf("More information at:\r\n\r\n");
        cprintf("    https://nikkhokkho.sourceforge.io/static.php?page=ZEROFILL\r\n");
        cprintf("Press ENTER to continue...");
        getch();
        cprintf("\r\n");
    }
    return(0);
}
